# **STEPS** #
## 1. Install Git in your system Globally *(if not presnent)*
  
[READ HERE FOR COMPLETE INSTALLATION GUIDE](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## 2. Open Folder in command Prompt or teminal where you want to download the template.

## 3. Click On Clone option from left side of this web panel and make sure in drop down HTTPS is selected now copy that command and paste in terminal or command Prompt

## 4. Close Termnial 

## 5. Install ##

* **Node.js**
* **NPM**
* **BOWER**

Than go in app/ open in termnal and type -> 

```
#!terminal

bower install
```
Once Done get out : Command 
```
#!Terminal

cd ../
```
 and type -> 
```
#!Terminal

npm install
```

### Installation part Done ###

**Execute** run 
```
#!Terminal

gulp
```
 command and Woah you are ready to make awesome website...

Once the website is completed in production 
 type gulp build for making project for production ready

-------------------------------------------------
* # **ALWAYS COMMIT AND MAINTAIN THE REPOSITORY** #