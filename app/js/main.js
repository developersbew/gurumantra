(function(){
  
$('.slider-contents').flexslider({
	 namespace: "jumbo-slider",
	 animation: "slide",
	 selector: ".slide-container > li",
	 directionNav: Modernizr.touchevents?false:true, 
});

$('.latest-news').flexslider({
	 namespace: "latest-news",
	 animation: "slide",
	 selector: ".latest-news-content > li",
	 itemWidth: 230,
	 itemMargin : 10,
	 directionNav: false,
	 controlNav: false,
	 pauseOnHover: true,
	 slideshowSpeed : 2000,
	 move: 1,
	 animationSpeed: 1000,
	 
});

$('.testimonials').flexslider({
	namespace: "testimonials",
	animation: "fade",
	selector: ".students-testimonials > li",
	directionNav: false,
	controlNav: false,
	pauseOnHover: true
});

$('.more-about-container li a').on("mouseenter",function(e){
	e.preventDefault();
	$('.more-about-container li a').removeClass('tab-title');
	$(this).addClass('tab-title');
	console.log('vikas');
	var got=$(this).attr('href');
	$('.more-about-content div').removeClass("tab-show");
	$(got).addClass("tab-show");
});

$('.more-about-content .mac-title').on("click",function(e){
	e.preventDefault();
	$('.more-about-content .mac-content').removeClass('res-tab-content');
	$(this).next().toggleClass('res-tab-content');

});

})();